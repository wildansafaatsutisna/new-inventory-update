import Vue from 'vue'
import Router from 'vue-router'
import login from '@/components/login'
import brand from '@/views/brand/brand'
import brandtutup from '@/views/brand/brandtutup'
import kategori from '@/views/kategori/kategori'
import type from '@/views/type/type'
import typetutup from '@/views/type/typetutup'
import masterbotol from '@/views/masterbotol/botol'
import supplier from '@/views/supplier/supplier'
import mastertutupbotol from '@/views/masterbotol/TutupBotol'
import pembeliansupplier from '@/views/supplier/pembeliansupplier'
import barangmasuk from '@/views/barangmasuk/barangmasuk'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/barangmasuk',
      name: 'barangmasuk',
      component: barangmasuk
    },
    {
      path: '/brand',
      name: 'brand',
      component: brand
    },
    {
      path: '/brandtutup',
      name: 'brandtutup',
      component: brandtutup
    },
    {
      path: '/supplier',
      name: 'supplier',
      component: supplier
    },
    
    {
      path: '/masterbotol',
      name: 'masterbotol',
      component: masterbotol
    },
    {
      path: '/mastertutupbotol',
      name: 'mastertutupbotol',
      component: mastertutupbotol
    },
    {
      path: '/kategori',
      name: 'kategori',
      component: kategori
    },
    {
      path: '/type',
      name: 'Type',
      component: type
    },
    {
      
      path: '/typetutup',
      name: 'typetutup',
      component: typetutup
    },
    {
      path: '/pembeliansupplier',
      name: 'pembeliansupplier',
      component: pembeliansupplier
    }
  ]
})
