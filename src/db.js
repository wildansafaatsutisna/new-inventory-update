import {initializeApp} from 'firebase'
 const app = initializeApp({
  apiKey: "AIzaSyDpvSUD1CoSCMh7ny5ZDZz1CyfsH-eJRy0",
  authDomain: "inventory-a8981.firebaseapp.com",
  databaseURL: "https://inventory-a8981.firebaseio.com",
  projectId: "inventory-a8981",
  storageBucket: "inventory-a8981.appspot.com",
  messagingSenderId: "929772307954"
  });
export const db = app.database();
export const storage = app.storage();
export const kategori = db.ref('kategori');
export const brand = db.ref('brand');
export const brandtutup = db.ref('brandtutup');
export const type = db.ref('type');
export const faktur = db.ref('faktur');
export const typetutup = db.ref('typetutup');
export const masterbotol = db.ref('masterbotol');
export const mastertutupbotol = db.ref('mastertutupbotol');
export const supplier = db.ref('supplier');
export const pembelianbotol = db.ref('pembelianbotol');




